﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace VirtualTour.UI
{
    public class MainMenu : MonoBehaviour
    {
        public GameObject MenuPanel;
        public GameObject AdditionalMaterialsPanel;
        public GameObject ManagementPanel;
        public GameObject TooltipPanel;
        public GameObject RestartPanel;
        public List<GameObject> BackgroundMenuPanels;
        public List<GameObject> GamePanels;
        public GameObject Aim;
        public GameObject FPC;
        public GameObject AnimationCamera;

        private AudioSource _menuAudio;
        private Image _background;

        private bool _isManagementRun;
        private bool _isFirstStartGame;
        private bool _isOnGame;

        public void Awake()
        {
            _menuAudio = GetComponent<AudioSource>();
            _background = GetComponent<Image>();
            RestartPanel.SetActive(false);
            CollapseAllLeaveStartMenu();
            FPC.SetActive(false);
            AnimationCamera.SetActive(true);
        }

        public void Update()
        {
            RunEscapeKey();
            RunF1Key();
        }

        public void CollapseAllLeaveStartMenu()
        {
            AdditionalMaterialsPanel.SetActive(false);
            ManagementPanel.SetActive(false);
            TooltipPanel.SetActive(false);

            MenuPanel.SetActive(true);
        }

        public void RunEscapeKey()
        {
            if (Input.GetKeyUp(KeyCode.Escape))
            {
                if (MenuPanel.activeSelf)
                {
                    ExitApplication();
                }
                else
                {
                    StopGame();
                }
            }
        }

        public void RunF1Key()
        {
            if (Input.GetKeyUp(KeyCode.F1))
            {
                if (_isOnGame)
                {
                    if (_isManagementRun)
                    {
                        ManagementPanel.SetActive(false);
                        TooltipPanel.SetActive(true);
                    }
                    else
                    {
                        ManagementPanel.SetActive(true);
                        TooltipPanel.SetActive(false);
                    }

                    _isManagementRun = !_isManagementRun;
                }
            }
        }

        public void ExitApplication()
        {
            Application.Quit();
        }

        public void StartGame()
        {
            SetActiveBackground(false);
            MenuPanel.SetActive(false);
            AdditionalMaterialsPanel.SetActive(false);
            _isFirstStartGame = true;
            AnimationCamera.SetActive(false);
            FPC.SetActive(true);
            TooltipPanel.SetActive(true);
            _isOnGame = true;
            _menuAudio.enabled = false;
        }

        public void StopGame()
        {
            MenuPanel.SetActive(true);
            RestartPanel.SetActive(true);
            SetActiveBackground(true);
            AnimationCamera.SetActive(true);
            FPC.SetActive(false);
            TooltipPanel.SetActive(false);
            _isOnGame = false;
            _menuAudio.enabled = true;
        }

        public void GoToAdditionMaterials()
        {
            if (AdditionalMaterialsPanel.activeSelf)
            {
                AdditionalMaterialsPanel.SetActive(false);
            }
            else
            {
                AdditionalMaterialsPanel.SetActive(true);
            }
        }

        public void RestartGame()
        {
            SceneManager.LoadScene(0);
        }

        private void SetActiveBackground(bool value)
        {
            if (value)
            {
                _background.enabled = true;
                foreach (var item in BackgroundMenuPanels)
                {
                    item.SetActive(true);
                }
                foreach (var item in GamePanels)
                {
                    item.SetActive(false);
                }

                Aim.SetActive(false);
            }
            else
            {
                _background.enabled = false;
                foreach (var item in BackgroundMenuPanels)
                {
                    item.SetActive(false);
                }
                foreach (var item in GamePanels)
                {
                    item.SetActive(true);
                }

                Aim.SetActive(true);
            }
        }
    }
}
